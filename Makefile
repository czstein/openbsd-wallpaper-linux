install:
	mkdir -p ${D}/usr/bin
	mkdir -p ${D}/usr/share
	cp -f openbsd-wallpaper ${D}/usr/bin/
	cp -fr openbsd-backgrounds ${D}/usr/share/
